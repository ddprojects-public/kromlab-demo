let listAvatarImages = ['applicant.jpg', 'applicant1.jpg', 'applicant.jpg', 'applicant1.jpg', 'applicant1.jpg', 'applicant1.jpg', 'applicant1.jpg','applicant.jpg',
'applicant.jpg', 'applicant.jpg', 'applicant.jpg', 'applicant.jpg', 'applicant1.jpg','applicant.jpg', 'applicant1.jpg', 'applicant1.jpg', 'applicant1.jpg',];

let page = (function (){
    initAvatarBlackLayer = () => {
        let selector = $('.applicants__avatar-wrapper');
        if (selector.hasClass('layer-black')) {
            selector.removeClass('layer-black')
        }
        selector.last().addClass('layer-black')
    };

    this.initAvatar = (n) => {
        $('.applicants__avatar-wrapper').remove(); // clear all
        for (let i=0; i<n && i<listAvatarImages.length; i++) {
            let template = `
                <div class="applicants__avatar-wrapper mr-1">
                    <img src="./img/${listAvatarImages[i]}" class="rounded-circle w-100">
                </div>
            `;
            $('.js-applicants__avatar').append(template);
        }      
        initAvatarBlackLayer();  
    }

    this.initAvatarSection = () => {
        let box_w = $('.applicants__wrapper').width();
        initAvatar(Math.floor(box_w / 44)); // 44px per item (width 40 + mr-1)
    }

    this.onBrowserChanged = () => {
        $(window).resize(function(){
            this.initAvatarSection();
        });
    }

    this.initPage = () => {
        this.initAvatarSection();
        this.onBrowserChanged();
    }

    return this;
})();

$(() => {
    page.initPage();
});